import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class testConn {

	public static void main(String[] args) {

		Connection conn = null;
	
		try {
		    conn =
		       DriverManager.getConnection("jdbc:mysql://localhost/ecommerce?" +
		                                   "user=root&password=");

		    System.out.println("OK");
		} catch (SQLException ex) {
		    // handle any errors
		    System.out.println("SQLException: " + ex.getMessage());
		    System.out.println("SQLState: " + ex.getSQLState());
		    System.out.println("VendorError: " + ex.getErrorCode());
		}		
				
	}

}
