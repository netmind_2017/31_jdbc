import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Category {
	
	//variables
	private int idCategory;
	private String name;
	
	//Constructor
	public Category(){
		
	}
	
	//encapuslation
	public String GetName()
	{
		return this.name;
	}
	
	public void SetNane(String name)
	{
		this.name = name;;
	}
	

	public  List<Category> GetCategory() {
		try{
			
			Connection conn = null;
			
		    Statement stmt = null;
		       
		    conn =  DriverManager.getConnection("jdbc:mysql://localhost/ecommerce?user=root&password=");
		   
		    //Execute a query
		    stmt = conn.createStatement();
		     
			//SQL STRING
			String sql  = "SELECT * from categories";
			
			// execute the query, and get a java resultset
			ResultSet rs = stmt.executeQuery(sql);
	       
	        //create list product
			List<Category> categories = new ArrayList<Category>();
	        
	        while(rs.next())
	        {		        
	        	Category item =new Category();

	        	item.idCategory = Integer.parseInt(rs.getString("idCategory"));
	        	item.name = rs.getString("name");	 
	        	categories.add(item);
	        }
	        
	        //close recordset
	        rs.close();
	        
	        return categories;
	    }
	    catch(Exception ex)
	    {
	    	System.out.println(ex.getMessage());
	    	return null;
	    }
	}

}




